@if (Auth::guest())
@else
<table class="table" id="table"
                data-toggle="table"
                class="table table-bordered table-striped table-condensed"
                data-pagination="true"
                data-click-to-select="true"
                data-search="true"
                data-advanced-search="true"
                data-id-table="advancedTable"
                data-show-export="true"
                data-sort-order="desc">
                <thead class="thead-dark">
                <tr>
                    <th></th>
                    <th>
                        {{ trans('etudiant.nom') }}
                    </th>
                    <th>
                        {{ trans('etudiant.prenom') }}
                    </th>
                    <th>
                        {{ trans('commun.actions') }}
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($etudiants as $etudiant)
                    <tr>
                        <td class="col-sm-2">
                        <a href="{{ route('showEtudiant',$etudiant->id) }}">{{ $etudiant->id }}</a>                         
                        </td>
                        <td class="col-sm-2">
                            {{ $etudiant->nom}}
                        </td>
                        <td class="col-sm-2">
                            {{ $etudiant->prenom }}
                        </td>
                        <td class="col-sm-2">
                            <div>
                            <div id="option{{$etudiant->id}}o">
                                <button id="modifier" name='modifier' type="button" onclick='modifier("{{route('editEtudiant',$etudiant->id) }}")' class="btn btn-primary" data-toggle="modal" data-target="#modalTable">{{ trans('commun.modifier') }}</button>
<button id='bouton' name='bouton' type="button" onclick='send("{{route('confirmEtudiant',$etudiant->id) }}","#option{{$etudiant->id}}")' class="btn btn-default btn-danger">
{{ trans('commun.supprimer') }}</button>
                            </div>
                            <div id="option{{$etudiant->id}}">
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
@endif
                