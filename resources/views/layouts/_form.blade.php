<!DOCTYPE html>
<html lang="en">

<head>
</head>
<body>
            <div class="modal-header">
                <h4 class="modal-title">
                    @yield('titre')
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                </h4>  
            </div> 
            <div class="modal-body modal-body-custom">  
            <div class="alert hidden" id="alert">{{ session('status') }}</div>
                <div class="container-fluid" id="content-region" style="padding-right: 0px;padding-left: 0px;">
                    @yield('content')
                </div>
            </div>
            <div class="modal-footer">
                        <button id="close" type="button" class="btn btn-default" data-dismiss="modal">{{ trans('commun.fermer') }}</button>
                    </div>

<script type="text/javascript">
  $(function(){
    $('input').click(function(){
        $('#alert').addClass('hidden');
    });
    $('#registerForm').submit(function(e) {
      e.preventDefault()
      var $form = $(this)
      $.post($form.attr('action'), $form.serialize())
      .done(function(data) {
        $('#alert').html(data)
        $('#alert').addClass('alert-success');
        $('#alert').removeClass('hidden');
        $("#alert").css('color','green');
        $("#alert").css('font-weight','bold');
        $("#alert").fadeOut("slow",function(){
                    $(this).fadeIn("slow");
                     });
        // nous recuperons la valeur de la page courante avec $cp dans la page precedente
        $("#table").load('{{ route("tableChange") }}?page='+$cp);
        function redir(){
       // self.location.href="home"
        $('#alert').addClass('hidden');
        if($action=="ajouter"){
        $('input[name="nom"]').val('');
        $('input[name="prenom"]').val('');}
        }
        setTimeout(redir,1500)
      })
      .fail(function(data) {
        $.each(data.responseJSON, function (key, value) {
                    $('#alert').html(value);
                    $('#alert').removeClass('hidden');
                    $("#alert").css('color','red');
                    $("#alert").css('font-weight','bold');
                    $("#alert").fadeOut("slow",function(){
                    $(this).fadeIn("slow");
                     });
        })
      })
    });
});
</script>
</body>

</html>