@extends('layouts._form')
@section('titre')
<label class="form_titre">
@if(isset($etudiant))
{{ trans('etudiant.titremodification') }}
@else
{{ trans('etudiant.titreajout') }}
@endif
</label>
@endsection
@section('content')
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif
@if(isset($etudiant))
{!! Form::model($etudiant, ['route' => ['updateEtudiant', $etudiant->id], 'method' => 'put','id'=>'registerForm']) !!}
@else
{!! Form::open(['route'=>'storeEtudiant','id'=>'registerForm']) !!}
@endif

{!! Form::label("{{ trans('etudiant.nom') }}", trans('etudiant.nom')) !!}
{!! Form::text('nom') !!}
{!! Form::label("{{ trans('etudiant.prenom') }}", trans('etudiant.prenom')) !!}
{!! Form::text('prenom') !!}
@if(isset($etudiant))
{!! Form::submit(trans('commun.enregistrer'),array('class'=>'btn btn-sm btn-primary m-t-n-xs')) !!}
@else
{!! Form::submit(trans('commun.ajouter'),array('class'=>'btn btn-sm btn-primary m-t-n-xs')) !!}
@endif
{!! Form::close() !!}
@endsection