@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><label class="form_titre">{{ trans('etudiant.information')}}</label></div>

                <div class="panel-body">
                    <table class="table table-bordered table-striped table-condensed">
                        <tr>
                            <th><label class="control-label">{{ trans('etudiant.nom') }}</label></th>
                            <td><label class="control-label">{{ $etudiant->nom }}</label></td>
                        </tr>
                        <tr>
                            <th><label class="control-label">{{ trans('etudiant.prenom') }}</label></th>
                            <td><label class="control-label">{{ $etudiant->prenom }}</label></td>
                        </tr>
                        </table>
                        <br/>
                        <div class="col-sm-6">
                            <a class="btn btn-default btn-primary" href="{{route('home') }}">{{trans('commun.accueil')}}</a>
                        </div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection