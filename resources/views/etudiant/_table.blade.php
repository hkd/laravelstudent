<div id="tableau-form" style="margin-top:10px">
        <div id="tableau_content" class="col-sm-12">

			<table class="table" id="table"
                data-toggle="table"
                class="table table-bordered table-striped table-condensed"
                data-pagination="true"
                data-click-to-select="true"
                data-search="true"
                data-advanced-search="true"
                data-id-table="advancedTable"
                data-show-export="true"
                data-sort-order="desc">
                <thead class="thead-dark">
				<tr>
					<th></th>
					<th>
						{{ trans('etudiant.nom') }}
					</th>
					<th>
						{{ trans('etudiant.prenom') }}
					</th>
					<th>
						{{ trans('commun.actions') }}
					</th>
				</tr>
				</thead>
				<tbody>
					@foreach ($etudiants as $etudiant)
					<tr>
						<td class="col-sm-1">
						<a href="{{ route('showEtudiant',$etudiant->id) }}">{{ $etudiant->id }}</a>							
						</td>
						<td class="col-sm-2">
							{{ $etudiant->nom}}
						</td>
						<td class="col-sm-2">
							{{ $etudiant->prenom }}
						</td>
						<td class="col-sm-2">
							<div>
							<div id="option{{$etudiant->id}}o">
								<button id="modifier" name='modifier' type="button" onclick='modifier("{{route('editEtudiant',$etudiant->id) }}")' class="btn btn-primary" data-toggle="modal" data-target="#modalTable">{{ trans('commun.modifier') }}</button>
								<button id='bouton' name='bouton' type="button" onclick='send("{{route('confirmEtudiant',$etudiant->id) }}","#option{{$etudiant->id}}")' class="btn btn-default btn-danger">
								{{ trans('commun.supprimer') }}

							</div>
							<div id="option{{$etudiant->id}}">
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

	<div class="form-group">
				<button onclick="last()" class="btn btn-default col-sm-2">{{ trans('pagination.previous')}}</button>
                    <label class="col-sm-8 control-label text-center" id="page"></label>
                <button onclick="next()" class="btn btn-default col-sm-2"> {{ trans('pagination.next')}}</button>
             </div>

		<div class="form-group">
		<button id="ajouter" type="button" class="btn btn-default btn-success col-sm-1" data-toggle="modal" data-target="#modalTable" onclick="ajouter()" style="top: 10px;">
    	<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </button>
    	</div>
</div>
<div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false">
    <div class="modal-dialog">
        <div id="content_dialog" class="modal-content"> 
         </div>
    </div>
</div> 
<script type="text/javascript">
	var $ii;
	var $coinn;
	var $action;
	var $cp={{ $etudiants->currentPage() }};
	function send($i,$coin) {
	    fermer();
	    $ii=$i;
	    $coinn=$coin;
	    $($coin).load($i);
	    $($coin).show();
	}
	function pageInit(){
        $("#table").load('{{ route("tableChange") }}?page='+$cp);
        $('#page').text("Page "+$cp+"/"+{{ $etudiants->perPage() }});
	}
	function last(){
		if($cp!=1)$cp=$cp-1;
		pageInit();
	}

	function next(){
		if($cp!={{ $etudiants->perPage() }})$cp=$cp+1;
		pageInit();
	}
	function fermer() {
    $($coinn).hide();
    }
    function fermera() {
    $($coinn+'o').addClass('hidden');
    }
	function modifier($i){
		$action="modifier";
   $('#content_dialog').load($i);
	}
	function ajouter(){
		$action="ajouter";
   $('#content_dialog').load("{{ route('addEtudiant') }}");
	}
	
	
</script>