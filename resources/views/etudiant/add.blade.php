@extends('layouts._form')
@section('titre')
{{ trans('etudiant.titreajout') }}
@endsection
@section('content')
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif
					@include('etudiant._form')
@endsection