@extends('layouts._form')
@section('titre')
@if(isset($etudiant))
{{ trans('etudiant.titremodification') }}
@else
{{ trans('etudiant.titreajout') }}
@endif
@endsection
@section('content')
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif
					@include('etudiant._form')
@endsection