<p></p>
<div id="mes" class="hidden"></div>
<table id="tt">
    <tr><td>
{!! Form::open(['route'=>array('deleteEtudiant','id'=>$id),'method'=>'delete','id'=>'confirmForm']) !!}
<input type="submit" value="{{ trans('commun.oui') }}" class="btn-danger">{!! Form::close() !!}</td>
<td><button type="button" onclick="fermer()">{{ trans('commun.non') }}</buttton>
</td></tr>
</table>
<script type="text/javascript">
  $(function(){
    $('#confirmForm').submit(function(e) {
      e.preventDefault()
      var $form = $(this)
      $.post($form.attr('action'), $form.serialize())
      .done(function(data) {
      	$('#tt').addClass('hidden');
        $('#mes').html(data)
        $('#mes').removeClass('hidden');
        $("#mes").css('color','green');
        $("#mes").css('font-weight','bold');
        $("#mes").fadeOut("slow",function(){
                    $(this).fadeIn("slow");
                     });
        fermera();
        function redir(){
          $("#table").load('{{ route("tableChange") }}?page='+$cp);
        }
        setTimeout(redir,1500)
      })
      .fail(function(data) {
        $.each(data.responseJSON, function (key, value) {
            
                    var input = '#formRegister input[name=' + key + ']';
                    $('#mes').html(value);
                    $('#mes').removeClass('hidden');
                    $("#mes").css('color','red');
                    $("#mes").css('font-weight','bold');
                    $("#mes").fadeOut("slow",function(){
                    $(this).fadeIn("slow");
                     });
        })
      })
    });
});
</script>