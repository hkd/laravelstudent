<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

     'failed' => 'These credentials do not match our records.',
     'throttle' => 'Too many login attempts. Please try agin in: seconds seconds. ',
     'logintitre' => 'Enter your identifiers',
     'login' => 'Email Address',
     'rappel' => 'remember me',
     'motpasse' => 'Password',
     'motpasseconfirme' => 'Confirm password',
     'motoublie' => 'Password forgotten',
     'seconnecter' => 'login',
     'enregistrer' => 'Register',
     's_enregistrer'=>'Register',
     'nom' => 'Name',
     'deconnexion' => 'Disconnect',
];
