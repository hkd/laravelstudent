<?php

return [
	 'enregistrer' => 'Save',
	 'details'=>'Details',
	 'modifier' => 'Modify',
	 'supprimer' => 'Delete',
	 'accueil' => 'Home',
	 'actions' => 'Actions',
	 'espace' => 'Management Space',
	 'ajouter'=> 'Add',
	 'enregistrer' => 'Save',
	 'oui' => 'yes',
	 'non' => 'no',
	 'fermer' => 'Close',
];