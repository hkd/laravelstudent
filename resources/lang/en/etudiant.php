<?php

return [
	'nom' => 'Name',
    'prenom' => 'Firstname',
    'msgenregistrementok' => 'Register successfully!',
    'titremodification' => 'Edit student' ,
    'msgsupprimerok' => 'Delete successfully!',
    'msgmiseajourok' => 'The update has been successfully completed!',
    'information' => 'Information about the student',
    'liste' => 'The list of students',
    'nomrequired' => 'The name is required',
    'nomunique' => 'The name is unique',
    'nommax' => 'The name must not exceed 50 characters',
    'prenomrequired' => 'First name is required',
    'prenommax' => 'The first name must not exceed 50 characters',
    'titreajout' => 'Add a student',
    'projet' => 'Student Management',
];