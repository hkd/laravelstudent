<?php

return [
    'nom' => 'Nom',
    'prenom' => 'Prénom',
    'msgenregistrementok' => 'Enregistrement effectuer avec succès !',
    'titremodification' => 'Modifier étudiant' ,
    'msgsupprimerok' => 'Supprimer avec succès !',
    'msgmiseajourok' => 'La mise à jour a été effectué avec succès !',
    'information' => 'Information sur l\'étudiant',
    'liste' => 'La liste des etudiants',
    'nomrequired' => 'Le nom est obligatoire',
	'nomunique' => 'Le nom est unique',
    'nommax' => 'Le nom ne doit pas dépasser 50 caractère',
    'prenomrequired' => 'Le prénom est obligatoire',
    'prenommax' => 'Le prénom ne doit pas dépasser 50 caractère',
    'titreajout' => 'Ajouter un étudiant',
    'projet' => 'Gestion Etudiants'
];