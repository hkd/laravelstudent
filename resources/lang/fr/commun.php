<?php

return [
    'enregistrer' => 'Enregister',
    'details' => 'Détails',
    'modifier' => 'Modifier',
    'supprimer' => 'Supprimer',
    'accueil'=>'Accueil',
    'actions' => 'Actions',
    'espace' => 'Espace de gestion',
    'ajouter' => 'Ajouter',
    'enregistrer' => 'Enregistrer',
    'oui' => 'oui',
    'non' => 'non',
    'fermer' => 'Fermer'
];