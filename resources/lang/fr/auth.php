<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "Ces informations d'identification ne correspondent pas à nos enregistrements.",
    'logintitre' => 'Entrez Vos identifiants s.v.p',
    'login' => 'Adresse Email',
    'rappel' => 'se rappel de moi',
    'motpasse' => 'Mot de passe',
    'motpasseconfirme' => 'Confirmer le mot de passe',
    'motoublie' => 'Mot de passe oublié',
    'seconnecter' => 'S\'identifier',
    's_enregistrer' => 'S\'enregistrer',
    'enregistrer' => 'Enregistrer',
    'nom' => 'Nom',
    'deconnexion' => 'Se déconnecter',
    'throttles' => 'Trop de tentatives de connexion. Veuillez réessayer dans: seconds secondes',
];
