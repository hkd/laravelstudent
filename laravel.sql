-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 02 août 2018 à 10:46
-- Version du serveur :  5.7.21
-- Version de PHP :  7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `laravel`
--

-- --------------------------------------------------------

--
-- Structure de la table `etudiants`
--

DROP TABLE IF EXISTS `etudiants`;
CREATE TABLE IF NOT EXISTS `etudiants` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=187 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `etudiants`
--

INSERT INTO `etudiants` (`id`, `nom`, `prenom`, `created_at`, `updated_at`) VALUES
(104, 'Brennan', 'Jilliann', NULL, NULL),
(105, 'Gray', 'Charlotte', NULL, NULL),
(78, 'dao', 'Hamadouu', NULL, NULL),
(87, 'Dustin', 'Chastity', NULL, NULL),
(88, 'Declan', 'Karen', NULL, NULL),
(89, 'Moses', 'Jemima', NULL, NULL),
(91, 'Nash', 'Illiana', NULL, NULL),
(92, 'Ishmael', 'Mirandaa', NULL, NULL),
(93, 'Alvin', 'Madalinee', NULL, NULL),
(94, 'Tarik', 'Stephanie', NULL, NULL),
(95, 'Dexter', 'Mari', NULL, NULL),
(97, 'Julian', 'Mira', NULL, NULL),
(99, 'Wade', 'Martena', NULL, NULL),
(100, 'Carl', 'Keelie', NULL, NULL),
(101, 'Reed', 'Lilah', NULL, NULL),
(102, 'Dolan', 'Brynne', NULL, NULL),
(103, 'Dale', 'Kylie', NULL, NULL),
(106, 'Dieter', 'Leigh', NULL, NULL),
(107, 'Elijah', 'Karyn', NULL, NULL),
(108, 'Brock', 'Maia', NULL, NULL),
(109, 'Griffith', 'Martha', NULL, NULL),
(110, 'Scott', 'Idona', NULL, NULL),
(111, 'Wylie', 'Lana', NULL, NULL),
(112, 'Zahir', 'Lenore', NULL, NULL),
(113, 'Porter', 'Dai', NULL, NULL),
(114, 'Fulton', 'Kameko', NULL, NULL),
(115, 'Cullen', 'Ramonaa', NULL, NULL),
(116, 'Lucian', 'Nevada', NULL, NULL),
(117, 'Cullen', 'Hanna', NULL, NULL),
(118, 'Colton', 'Quemby', NULL, NULL),
(119, 'Sawyer', 'Kylie', NULL, NULL),
(120, 'Herman', 'Kimberly', NULL, NULL),
(121, 'Carter', 'Heidi', NULL, NULL),
(122, 'Nero', 'Alma', NULL, NULL),
(123, 'Garrison', 'Aileen', NULL, NULL),
(124, 'Gil', 'Noel', NULL, NULL),
(125, 'Coby', 'Naida', NULL, NULL),
(126, 'Channing', 'Anjolie', NULL, NULL),
(127, 'Drew', 'Serina', NULL, NULL),
(128, 'Dylan', 'Guinevere', NULL, NULL),
(129, 'Hamish', 'Latifah', NULL, NULL),
(130, 'Kieran', 'Quin', NULL, NULL),
(131, 'Adrian', 'Claudia', NULL, NULL),
(132, 'Drew', 'Cailin', NULL, NULL),
(133, 'Dominic', 'Keely', NULL, NULL),
(134, 'Alan', 'Nola', NULL, NULL),
(135, 'Devin', 'Anastasia', NULL, NULL),
(136, 'Finn', 'Kelly', NULL, NULL),
(137, 'Asher', 'Bryar', NULL, NULL),
(138, 'Steel', 'Jessamine', NULL, NULL),
(139, 'Ahmed', 'Karen', NULL, NULL),
(140, 'Fitzgerald', 'Cassandra', NULL, NULL),
(141, 'Joshua', 'Shellie', NULL, NULL),
(142, 'Buckminster', 'Jasmine', NULL, NULL),
(143, 'Harrison', 'Judith', NULL, NULL),
(144, 'Quinlan', 'Victoria', NULL, NULL),
(145, 'Ali', 'Alexa', NULL, NULL),
(146, 'Dennis', 'Dara', NULL, NULL),
(147, 'Abbot', 'Uta', NULL, NULL),
(148, 'Grant', 'Rana', NULL, NULL),
(149, 'Jamal', 'Taylor', NULL, NULL),
(150, 'Rajah', 'Madison', NULL, NULL),
(151, 'Davis', 'Lois', NULL, NULL),
(152, 'Merrill', 'Mechelle', NULL, NULL),
(153, 'Travis', 'Kimberly', NULL, NULL),
(154, 'Jin', 'Heidi', NULL, NULL),
(155, 'Geoffrey', 'Jennifer', NULL, NULL),
(156, 'Lucius', 'Margaret', NULL, NULL),
(157, 'Evan', 'Belle', NULL, NULL),
(158, 'Samson', 'Fallon', NULL, NULL),
(159, 'Graham', 'Zephr', NULL, NULL),
(160, 'Noble', 'Delilah', NULL, NULL),
(161, 'Kasper', 'Melyssa', NULL, NULL),
(162, 'Omar', 'Doris', NULL, NULL),
(163, 'Wallace', 'Denise', NULL, NULL),
(164, 'Herman', 'Velma', NULL, NULL),
(165, 'Howard', 'Chava', NULL, NULL),
(166, 'Knox', 'Wynne', NULL, NULL),
(167, 'Jason', 'Galena', NULL, NULL),
(168, 'Reuben', 'Paloma', NULL, NULL),
(169, 'Beau', 'Paloma', NULL, NULL),
(170, 'Dean', 'Candace', NULL, NULL),
(171, 'Vance', 'Maite', NULL, NULL),
(172, 'Colorado', 'Quyn', NULL, NULL),
(173, 'Rudyard', 'Martina', NULL, NULL),
(174, 'Norman', 'Maxine', NULL, NULL),
(175, 'Hunter', 'Suki', NULL, NULL),
(176, 'Kennan', 'Maxine', NULL, NULL),
(177, 'Duncan', 'Erin', NULL, NULL),
(178, 'Cole', 'Carolyn', NULL, NULL),
(179, 'Ashton', 'Lydia', NULL, NULL),
(180, 'Lucian', 'Kimberly', NULL, NULL),
(181, 'Abbot', 'Joan', NULL, NULL),
(182, 'Hayes', 'Aurora', NULL, NULL),
(183, 'August', 'Gay', NULL, NULL),
(184, 'Bradley', 'Jordan', NULL, NULL),
(185, 'Dillon', 'Kyla', NULL, NULL),
(186, 'Colt', 'Cameron', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_15_162311_create_etudiants_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hamadou DAO', 'daohamadou@gmail.com', '$2y$10$ogl0R45FqDWyLzd0eH8XT.oczPZDdDNfEq3pQI.HATwuFTxGECrEW', '19iblPtZMmjG2HAUQNl0JXWUTRB8dv14m9OOBIGscJUOE7pRaFWESVUqQCbw', '2018-07-15 17:43:54', '2018-07-15 17:43:54'),
(2, 'dao', 'dao@g.com', '$2y$10$9V7h6g9XtHUg1ZB7Q9BB/uIgPxq/rDenGa4yj7xhmGIKqtkn5j3eS', 'RQempj5LKvRZF9zgPUGoIPVxY4jPgZP8cQHu6hiGQMvEY8xA9PghKj4Js8qm', '2018-07-23 17:20:19', '2018-07-23 17:20:19');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
