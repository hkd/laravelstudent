<?php

/**
 * @Auteur Hamadou DAO
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tableChange', 'HomeController@tableChange')->name('tableChange');
Route::get('/lang/{fr}', 'HomeController@lang')->name('lang');
Route::get('/langen', 'HomeController@langen')->name('langen');

Route::get('etudiants/add','EtudiantController@add')->name('addEtudiant');
Route::post('etudiants/store','EtudiantController@store')->name('storeEtudiant');
Route::get('etudiants/show/{id}','EtudiantController@show')->name('showEtudiant')->where('id','[0-9]+');
Route::get('etudiants/edit/{id}','EtudiantController@edit')->name('editEtudiant')->where('id','[0-9]+');
Route::put('etudiants/update/{id}','EtudiantController@update')->name('updateEtudiant')->where('id','[0-9]+');
Route::delete('etudiants/delete/{id}','EtudiantController@delete')->name('deleteEtudiant')->where('id','[0-9]+');
Route::get('etudiants/confirm/{id}','EtudiantController@confirm')->name('confirmEtudiant')->where('id','[0-9]+');
