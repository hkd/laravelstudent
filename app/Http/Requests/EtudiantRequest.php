<?php

/**
 * @Auteur Hamadou DAO
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EtudiantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|string|unique:etudiants|max:50',
            'prenom' => 'required|string|max:50',
        ];
    }

    public function messages()
    {
        return [
            'nom.require' => trans('etudiant.nomrequired'),
            'nom.unique' => trans('etudiant.nomunique'),
            'nom.max' => trans('etudiant.nommax'),
            'prenom.required' => trans('etudiant.prenomrequired'),
            'prenom.max' => trans('etudiant.prenommax'),
            'validation.required' => 'okk',
        ];
    }
}
