<?php
/**
 * @Auteur Hamadou DAO
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Etudiant;
use App;
use Config;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $etudiants = Etudiant::paginate(10); // recupérer toutes les donnnées enregistrées dans la table étudiants
        return view('home',compact('etudiants'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function tableChange()
    {
        $etudiants = Etudiant::paginate(10); // recupérer toutes les donnnées enregistrées dans la table étudiants
        return view('chargeTable',compact('etudiants'));
    }
    public function lang($lang){
        App::setLocale($lang);
        app()->setLocale($lang);
        Config::set('app.local', 'fr');
        return back()->with('lang',app()->getLocale());
    }
    public function langen(){
        Session::put('lang','en');
        return back();
    }
}
