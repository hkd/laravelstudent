<?php
/**
 * @Auteur Hamadou DAO
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Etudiant;
use App\Http\Requests\EtudiantRequest;
use Collective\Html\FormBuilder;

class EtudiantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /*
     * Ajouter un étudiant.
     * @return sur la vue précédente
     */
    public function add()
    {
        return view('etudiant._form');//back()->with('status',trans('etudiant.msgenregistrementok'));
    }

    /*
     * Ajouter un étudiant.
     * @return sur la vue précédente
     */
    public function store(EtudiantRequest $request)
    {
            if(Etudiant::create($request->all()));
            return trans('etudiant.msgenregistrementok');
            return back()->with('status',trans('etudiant.msgenregistrementerror'));

    }

    /**
     * Afficher les informations d'un étudiant.
     *
     * @param int $id : id de l'étudiant
     * @return view : show
     */
    public function show($id){
        $etudiant =Etudiant::findorfail($id);

        return view('etudiant.show',compact('etudiant'));
    }
    /**
     * Afficher le formulaire pour la modification.
     * 
     * @param int $id : id de l'étudiant
     * @return view : Vue Edit d'étudiant
     */
    public function edit($id){
        $etudiant = Etudiant::findorfail($id); //si l'id n'existe pas, il retourne une erreur 404

        return view('etudiant._form',compact('etudiant'));
    }

    /**
     * Valider la modification sur le formulaire.
     *
     * @param int $id (id de l'étudiant) , request ce qui a été posté les inputs
     * @return sur la vue précédente
     */
    public function update(Request $request,$id){
        $etudiant = Etudiant::findorfail($id);//si l'id n'existe pas, il retourne une erreur 404
        $etudiant->nom = $request->input('nom');
        $etudiant->prenom = $request->input('prenom');
        $etudiant->save();
        return trans('etudiant.msgmiseajourok');
    }
    

    /**
     * fenetre de confirmation pour une suppression
     *
     * @param int $id (id de l'étudiant) , request ce qui a été posté les inputs
     * @return sur la vue précédente
     */
    public function confirm(Request $request,$id){
        Etudiant::findorfail($id);
        return view('etudiant.confirm',compact('id'));
    }

    public function delete(Request $request,$id){
        Etudiant::destroy($id);//si l'id n'existe pas, il retourne une erreur 404
        return trans('etudiant.msgsupprimerok');
    }

}
